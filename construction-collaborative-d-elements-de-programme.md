# Construction collaborative d'éléments de programme

## Présentation

Ceci est une invitation aux collectif souhaitant proposer des éléments
de politique publique concrets et réalisables pour les élections municipales
de 2020. Les associations ayant une expertise du domaine et technique,
ainsi que les listes citoyennes sont les collectifs considérés dans cette
première version, pour agrandir le groupe, vous êtes invité·e·s à faire
des propositions : l'objectif est que l'invitation convienne au mieux
aux personnes concernées.

La méthode proposée est de choisir des formats de collaboration pour écrire
les éléments de politique publique que soutiennent ces collectifs. Les écrire
ensemble permettra de leur donner du poids ainsi que de les travailler de manière
approfondie pour que ces propositions soient complètes et activables.

Adopter un processus de collaboration similaire à celui qu'il est souhaitable 
de mettre en place au sein de la municipalité pour faire de la démocration
participative une réalité permettra de se faire à ces mécanismes, et de cette
manière, la mise en place au niveau de la municipalité sera déjà une deuxième
version plus rôdée.

Les méthodes d'établissement de programme basés sur un système pyramidal
entrainent des prises de décision ne satisfaisant que les intérêts des
personnes situées en haut du système pyramidal. Les attentes des personnes
situées en bas de la pyramide sont ignorées. L'objectif ici est de lutter contre
ces travers, en employant des méthodes de collaboration et de démocratie
participative mises en place par des collectifs travaillant sur ces méthodes
depuis plusieurs années (et siècles ;) )

## Mécanique

1. Adopter un ou des mécanismes d'échange et de prise de décision
2. Alimenter les échanges pour explorer toutes les facettes d'une question,
3. Collecter toutes les données factuelles sur une question
4. Rédiger collectivement des éléments de programme basés sur ces faits
5. Décider collectivement de l'adoption d'un élément de programme

Lorsque deux positions sont incompatibles, choisir en commun parmi :
- le consentement : tant que quelqu'un·e peut vivre avec, pas de blocage
- le consensus : l'adoption a lieu lorsque l'intégralité est choisie à 100%
par chaque participant·e
- le fork : scinder le groupe en deux, et chaque nouveau groupe

En amont, les collectifs sont invités à apporter les éléments qui ont déjà été
travaillés en interne, afin de construire sur les travaux réalisés au cours
des années écoulées.

## Éléments de programme

À l'issue de cette session, différents éléments de programme peuvent être
publiés.

Il n'est pas nécessaire qu'il y ait consensus sur tout,

et plusieurs programmes peuvent être publiés, avec le nom des assos qui le
soutiennent.

Ensuite, chaque asso pourra choisir de soutenir le programme qui lui correspond.


## Place de la démocratie participative

La proposition est ici d'adopter des formats de coopération 
et de construire en commun des éléments de programme adoptés collectivement par
les participant·e·s.

## Lien avec l'université populaire d'administration municipale

Les sessions de partage de savoirs et savoir-faire permettent d'être libre d'explorer
à un rythme qui n'est pas dicté par les contraintes extérieures. Cette 

## Participant·e·s

Pour la session de préfiguration, il faut être raisonnable sur ce qui est faisable.

Une proposition est que participent les associations et les groupes souhaitant
porter une liste municipale citoyenne.


## Méthodes de co-construction et de pris décision

De nombreuses méthodes de co-construction et de prise de décision existent,
et le groupe est invité à les choisir pour leurs qualités et les limites qu'on
leur connaît.

- méthodes actives de l'éducation populaire politique
- prise de décision liquide.
- holacratie.

## Groupes ressources

- Euroasis
- Alternatiba
- parti pirate
- ma voix
