# Université populaire d'administration d'une municipalité — Strasbourg

## Présentation

De nombreuses personnes estiment qu'il y a des décisions politiques correspondant
aux attentes de la société civile qui doivent être prises sans attente
au risque de souffrir des conséquences de l'inaction. Ces politiques sont
parfois contraires aux intérêts des groupes ne partageant pas d'intérêt avec la 
société civile, ce qui exige donc que les élus soient membres de la société civile.
Le niveau d'une municipalité est estimée comme étant une taille pertinente pour agir.
[cf contexte](#un-contexte-qui-mène-la-société-civile-à-vouloir-sautogérer)

(à noter que d'autres personnes ne partageront pas ce constat, cette proposition
ne vise pas satisfaire tout le monde)

Cependant, une liste citoyenne est-elle assez compétente pour assurer l'administration
d'une municipalité, en évitant les pièges dans lesquels peuvent tombent les équipes
municipales ? [cf "en est-on capables ?"](#en-est-on-capables-)

Ceci est un manifeste ouvert à la contribution, décrivant ce que pourra être une
université populaire donnant la capacité de participer à l'administration d'une municipalité.
[cf proposition](#organisons-une-université-populaire-sur-l-administration-municipale)

Vous êtes légitime pour contribuer, proposez des modifications de ce manifeste !
[cf Comment contribuer](CONTRIBUTING.md)

Et venez participer à cette université populaire !

## Programme

[Consultez le programme](programme.md)

Mise en pratique : [co-construction d'éléments de programme](https://gitlab.com/universite-populaire-municipale-strasbourgeoise/manifeste/blob/master/construction-collaborative-d-elements-de-programme.md)

## Comment contribuer

[Guide de contribution](CONTRIBUTING.md)

## Gestion collaborative du projet

[Gestionnaire d'issues](https://gitlab.com/universite-populaire-municipale-strasbourgeoise/manifeste/boards/891575)

## Manifeste

### Un contexte qui mène la société civile à vouloir s'autogérer

Différents groupes font le constat que des problèmes de société nécessitent
d'agir sans attendre.

Cela concerne le climat (annonce en 2018 de la nécessité
d'agir drastiquement sous 2 ans), l'égalité sociale (mouvement des gilets jaunes),
l'accueil des migrants et la protection des personnes sans-abris.

De nombreux collectifs et associations agissent de manière effective sur ces sujets,
au point que ces groupes réalisent des missions de service public (action sociale
auprès des personnes sans-abris par exemple).

Il y a cependant des divergences d'intérêt entre les groupes issus de la société
civile et les défenseurs de politiques compatibles avec le capitalisme financier.

Par conséquent, plusieurs formations politiques issues de la société civile sont
en cours de constitution.

L'échelle d'une municipalité permet de toucher concrètement à ces questions
sans être soumis à des contraintes de taille trop importantes.

#### Et en particulier Strasbourg

Strasbourg est une ville stratégiquement très importante, avec une société civile
ayant mis en place des associations très développées.

### En est-on capables ?

Les problématiques auxquelles sont confrontées les équipes municipales sont multiples,
dont :

- la nécessité d'arbitrer entre des propositions
- la technicité des dossiers
- les incitations de la part de groupes d'intérêt de différents bords
- la légalité
- les processus administratifs

Il a été identifié dans les derniers temps que les élu·e·s lancé·e·s depuis peu dans 
la gestion administrative et légistlative pouvaient être quelque peu perdu·e·s.

Quels sont les savoirs et savoir-faires à acquérir afin de parvenir à gérer de manière
une municipalité de manière efficace _et_ humaine ?


### Organisons une université populaire sur la gestion municipale

D'ici les élections de 2020, et même tout au long de la mandature afin de faire perdurer
les bienfaits d'une éducation populaire sur les savoirs et savoir-faires qui
permettent aux décisions politiques d'être prises dans l'intérêt de la société civile.

#### Populaire !

Différents groupes s'organisent autour de programmes différents, très bien.
Cette université populaire est un commun, où toutes et tous sont invité·e·s à
participer, acquérir des savoirs et des savoirs-faires sans distinction de groupe
politique.

Si chacun·e d'entre nous souhaite une administration au service de la société
civile, faisons en sorte que l'ensemble des potentiel·le·s futur·e·s élu·e·s
soient autant capable d'assurer cette mission au service des intérêts de la 
société civile.

Cela commence dès l'écriture de ce manifeste, également [ouverte à la contribution](CONTRIBUTING.md)


#### Programme  pédagogique

1. Le plus important est de progresser ensemble, en interagissant, les rencontres
régulières seront les moments clefs de ce programme
2. Des personnes expérimentées sur un sujet seront invitées à transmettre leurs
3. savoirs et savoirs-faires
3. De nombreuses ressources sont également disponibles en ligne et hors-ligne,
collectons-les pour les mettre à disposition pour de l'étude autonome
4. Les spécificités locales seront prises en compte, afin d'adapter les savoirs
aux réalités du terrain


### Quels rôles émergeront ?

Engageons-nous à ce qu'à l'issue de ce programme,

- l'ensemble des candidat·e·s aux élections municipales
- les élu·e·s formant la majorité suite à ces élections
- les élu·e·s formant l'opposition 
- les citoyen·ne·s engagé·e·s à être observateurs les décisions politiques,
à faire des contributions et à assurer un contre-pouvoir

soient toutes compétentes pour assurer ces activités au service des intérêts
de la société civile dans son ensemble.

### Signataires

@jibe-b
