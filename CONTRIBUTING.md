# Comment contribuer

- [créer un compte](https://gitlab.com/users/sign_in#register-pane) (il est possible d'utiliser un compte google ou twitter)
- demander l'accès en allant sur la [page d'accueil](https://gitlab.com/universite-populaire-municipale-strasbourgeoise/manifeste)
et en y cliquant sur le bouton "Request access"

![cliquer sur le bouton demande d'accès](/images/request-access-gitlab.png)

Dès lors que l'accès vous a été donné (modération réalisée manuellement, veuillez excuser le possible délai)

- accéder au [brouillon (en édition collaborative)](https://gitlab.com/universite-populaire-municipale-strasbourgeoise/manifeste/blob/edition-collaborative/README.md)
- éditer le manifeste en cliquant sur le bouton "Éditer" ![cliquer sur le bouton éditer](/images/edit-gitlab-readme.png)
- sauvegarder les modifications en cliquant sur le bouton "Commit changes" ![cliquer sur le bouton commit changes](/images/gitlab-commit-changes.png)
- suivre les échanges à propos de la mise à jour du manifeste